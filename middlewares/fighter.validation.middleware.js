const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService')
const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation

    let { name, health, power, defense } = req.body
    const fighterKeys = Object.keys(fighter)
    const bodyKeys = Object.keys(req.body)
    const errorMessages = []
    if (!name) {
        errorMessages.push(`Name is required`)
    } else {
        const data = FighterService.search({ name: name })
        data && errorMessages.push("Name already exist")
    }

    if (!power) {
        errorMessages.push(`Power is required`)
    } else {
        (power <= 1 || power >= 100) && errorMessages.push("Power must be at least 1 and more than 100")
        isNaN(power) && errorMessages.push("Power must be a number")
    }

    if (health) {
        (health <= 80 || health >= 120) && errorMessages.push("Health must be at least 80 and more than 120")
    }
    if (!defense) {
        errorMessages.push(`Defense is required`)
    } else {
        (defense <= 1 || defense >= 10) && errorMessages.push("Defense must be at least 1 and more than 10")
        isNaN(defense) && errorMessages.push("Defense must be a number")
    }

    if (fighterKeys.length !== bodyKeys.length + 2 || bodyKeys.filter(item => !fighterKeys.includes(item)).length) {
        errorMessages.push("Incorrect fighter property")
    }

    if (errorMessages.length) {
        res.status("400").json({
            error: true,
            message: errorMessages
        })
    } else {
        next()
    }
}
const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update

    let { name, health, power, defense } = req.body
    const errorMessages = []
    const fighterKeys = Object.keys(fighter)
    const bodyKeys = Object.keys(req.body)

    if (req.body.hasOwnProperty("name")) {
        !name.length && errorMessages.push("Name cannot be empty")
        const data = FighterService.search({ name: name })
        data && errorMessages.push("Name already exist")
    }

    if (req.body.hasOwnProperty("health")) {
        (health <= 80 || health >= 120) && errorMessages.push("Health must be at least 80 and more than 120")
    }

    if (req.body.hasOwnProperty("power")) {
        (power <= 1 || power >= 100) && errorMessages.push("Power must be at least 1 and more than 100")
        isNaN(power) && errorMessages.push("Power must be a number")
    }

    if (req.body.hasOwnProperty("defense")) {
        (defense <= 1 || defense >= 10) && errorMessages.push("Defense must be at least 1 and more than 10")
        isNaN(defense) && errorMessages.push("Defense must be a number")
    }

    if (!Object.keys(req.body).length) {
        errorMessages.push("Nothing to update")
    }

    if (bodyKeys.filter(item => !fighterKeys.includes(item)).length) {
        errorMessages.push("Incorrect fighter property")
    }
    if (errorMessages.length) {

        res.status("400").json({
            error: true,
            message: errorMessages
        })
    } else {
        next()
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;