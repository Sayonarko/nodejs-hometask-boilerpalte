const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query
    if (res.data) {
        return res.status("200").json(res.data)
    }

    if (!res.data && req.baseUrl.includes("users")) {
        return res.status("404").json({
            error: true,
            message: "User not found"
        })
    }

    if (!res.data && req.baseUrl.includes("fighters")) {

        return res.status("404").json({
            error: true,
            message: "Fighter not found"
        })
    }

    if (!res.data && req.baseUrl.includes("fights")) {

        return res.status("404").json({
            error: true,
            message: "Fight not found"
        })
    }

    if (res.err) {

        return res.status("400").json({
            error: true,
            message: "Something went wrong"
        })
    }

    next();
}

exports.responseMiddleware = responseMiddleware;