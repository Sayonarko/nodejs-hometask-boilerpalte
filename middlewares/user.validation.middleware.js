const { user } = require('../models/user');
const UserService = require('../services/userService');


const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    let { firstName, lastName, email, phoneNumber, password } = req.body
    const errorMessages = []
    const userKeys = Object.keys(user)
    const bodyKeys = Object.keys(req.body)

    if (!firstName) {
        errorMessages.push(`Firstname is required`)
    }
    if (!lastName) {
        errorMessages.push(`Lastname is required`)
    }
    if (!email) {
        errorMessages.push(`Email is required`)
    } else {
        email = email.toLowerCase()
        !email.endsWith("@gmail.com") && errorMessages.push("Only Gmail is allowed")
        const data = UserService.search({ email: email })
        data && errorMessages.push("Email already exist")
    }

    if (!phoneNumber) {
        errorMessages.push(`Phonenumber is required`)
    } else {
        (isNaN(phoneNumber) || phoneNumber.length !== 13) && errorMessages.push("Incorrect phonenumber")
        !phoneNumber.startsWith("+380") && errorMessages.push("The phonenumber must be as +380XXXXXXX")
        const data = UserService.search({ phoneNumber: phoneNumber })
        data && errorMessages.push("Phonenumber already exist")
    }
    if (!password) {
        errorMessages.push(`Password is required`)
    } else {
        typeof password !== "string" && errorMessages.push("Password cannot consist only of numbers")
        password.length < 3 && errorMessages.push("The password must be at least 3 characters long")
    }

    if (userKeys.length !== bodyKeys.length + 1 || bodyKeys.filter(item => !userKeys.includes(item)).length) {
        errorMessages.push("Incorrect user property")
    }

    if (errorMessages.length) {
        res.status("400").json({
            error: true,
            message: errorMessages
        })
    } else {
        next()
    }
}
const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update

    let { firstName, lastName, email, phoneNumber, password } = req.body
    const errorMessages = []
    const userKeys = Object.keys(user)
    const bodyKeys = Object.keys(req.body)

    if (req.body.hasOwnProperty("firstName")) {
        !firstName.length && errorMessages.push("Firstname cannot be empty")
    }
    if (req.body.hasOwnProperty("lastName")) {
        !lastName.length && errorMessages.push("Lastname cannot be empty")
    }
    if (req.body.hasOwnProperty("email")) {
        email = email.toLowerCase()
        !email.endsWith("@gmail.com") && errorMessages.push("Only Gmail is allowed")
        const data = UserService.search({ email: email })
        data && errorMessages.push("Email already exist")
    }

    if (req.body.hasOwnProperty("phoneNumber")) {
        (isNaN(phoneNumber) || phoneNumber.length !== 13) && errorMessages.push("Incorrect phonenumber")
        !phoneNumber.startsWith("+380") && errorMessages.push("The phonenumber must be as +380XXXXXXX")
        const data = UserService.search({ phoneNumber: phoneNumber })
        data && errorMessages.push("Phonenumber already exist")
    }

    if (req.body.hasOwnProperty("password")) {
        typeof password !== "string" && errorMessages.push("Password cannot consist only of numbers")
        password.length < 3 && errorMessages.push("The password must be at least 3 characters long")
    }

    if (bodyKeys.filter(item => !userKeys.includes(item)).length) {
        errorMessages.push("Incorrect user property")
    }

    if (!Object.keys(req.body).length) {
        errorMessages.push("Nothing to update")
    }

    if (errorMessages.length) {
        res.status("400").json({
            error: true,
            message: errorMessages
        })
    } else {
        next()
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;