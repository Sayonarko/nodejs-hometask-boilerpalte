const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
    // OPTIONAL TODO: Implement methods to work with fights
    getAll() {
        const item = FightRepository.getAll();
        if (!item.length) {
            return null;
        }
        return item;
    }

    search(search) {
        const item = FightRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    create(data) {
        const item = FightRepository.create(data);
        if (!item) {
            return null;
        }
        return item;
    }

    update(id, dataToUpdate) {
        const item = FightRepository.update(id, dataToUpdate);
        if (!item) {
            return null;
        }
        return item;
    }

    delete(id) {
        const item = FightRepository.delete(id);

        if (!item.length) {
            return null;
        }
        return item;
    }
}

module.exports = new FightersService();